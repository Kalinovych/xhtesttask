﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PerformanceTest : MonoBehaviour {

	void Start () {
		StartCoroutine( TestsInitRoutine() );
	}

	IEnumerator TestsInitRoutine() {
		yield return new WaitForSeconds( 2f );

		RunsTests();
	}

	const int iters = 4096 * 4096;

	private void RunsTests() {
		var stopwatch = new Stopwatch();
		float res;

		stopwatch.Start();
		res = RunSinglePassTest();
		stopwatch.Stop();
		
		print( "Single pass: " + stopwatch.ElapsedMilliseconds + ", res: " + res );

		stopwatch.Restart();
		res = RunDoublePassTest();
		stopwatch.Stop();

		print( "Double pass: " + stopwatch.ElapsedMilliseconds + ", res: " + res );
	}

	private float RunSinglePassTest() {
		float result = 0;
		for (int i = 0; i < iters; i++) {
			result += PayloadA( i );
			result += PayloadB( i );
		}
		return result;
	}

	private float RunDoublePassTest() {
		float result = 0;
		for (int i = 0; i < iters; i++) {
			result += PayloadA( i );
		}
		for (int i = 0; i < iters; i++) {
			result += PayloadB( i );
		}
		return result;
	}

	private float PayloadA(int value) {
		return Mathf.Sin( value );
	}

	private float PayloadB(int value) {
		return Mathf.Sin( value ) + Mathf.Cos( value );
	}
}
