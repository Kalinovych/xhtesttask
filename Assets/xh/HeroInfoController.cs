﻿using UnityEngine;

public class HeroInfoController : MonoBehaviour {
	protected Camera _targetCamera;

	void Start() {
		_targetCamera = Camera.main;
	}

	void LateUpdate () {
		var camRotation = _targetCamera.transform.rotation;
		var lookPoint = transform.position + camRotation * Vector3.forward;
		transform.LookAt(lookPoint, camRotation * Vector3.up);
	}
}
