﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

//[RequireComponent(typeof(RawImage))]
public class AuraGenerator : MonoBehaviour {
	[DllImport("AuraGen")]
	private static extern float SetAuraSize(uint size);

	[DllImport("AuraGen")]
	private static extern float DrawAura(byte[] buffer, float t);

	private const int TEXTURE_SIZE = 128;

	[SerializeField]
	private Material _targetMaterial;

	private Texture2D auraTexture;

	private byte[] buffer;
	
	void Start () {
		auraTexture = new Texture2D( TEXTURE_SIZE, TEXTURE_SIZE, TextureFormat.ARGB32, false ) {
			wrapMode = TextureWrapMode.Clamp
		};

		_targetMaterial.mainTexture = auraTexture;
		
		InitAuraRenderer();
	}

	void Update() {
		RenderAuraTexture();
	}

	private void InitAuraRenderer() {
		const uint byteCount = TEXTURE_SIZE * TEXTURE_SIZE * 4;
		buffer = new byte[byteCount];
		SetAuraSize(TEXTURE_SIZE);
	}

	private void RenderAuraTexture() {
		DrawAura(buffer, Time.time);

		auraTexture.LoadRawTextureData(buffer);
		auraTexture.Apply();
	}
}
