﻿using System.Collections;
using UnityEngine;

public class HeroController : MonoBehaviour {
	public float Energy { get; set; } = 1f;

	[SerializeField]
	private Animator _animator;

	[SerializeField]
	private float _actionEnergyCost = 0.2f;

	[SerializeField]
	private PostEffectController _effectController;

	private float _eneryRegenRate = 0.2f;

	private bool isRegenerating = false;

	private Coroutine renegerationCoroutine;

	private static readonly string[] AttackTriggers = {
		"Attack3Trigger", "Attack6Trigger",
		"AttackKick1Trigger", "AttackKick2Trigger"
	};

	private static readonly string[] RollTriggers = {
		"RollForwardTrigger", "RollBackwardTrigger",
		"RollLeftTrigger", "RollRightTrigger"
	};

	private const string DeathTrigger = "Death1Trigger";
	
	public void Attack() {
		print("Attack");

		if (UseActionEnergy()) {
			_animator.SetTrigger(GetRandomItem(AttackTriggers));
			StartWorkout(0.7f);
		}
	}

	public void Die() {
		print("Die");

		if (UseActionEnergy()) {
			_animator.SetTrigger(DeathTrigger);
			StartWorkout(1.2f);
		}
	}

	public void Roll() {
		print("Roll");

		if (UseActionEnergy()) {
			_animator.SetTrigger(GetRandomItem(RollTriggers));
			StartWorkout(0.5f);
		}
	}

	protected string GetRandomItem(string[] items) {
		return items[Random.Range(0, items.Length)];
	}
	
	protected bool UseActionEnergy() {
		return UseEnergy(_actionEnergyCost);
	}

	protected bool UseEnergy(float energyRequest) {
		if (energyRequest > Energy) {
			print("Not enough energy!");
			Regenerate();
			return false;
		}

		StopRegeneration();

		Energy -= energyRequest;

		return true;
	}

	private void StartWorkout(float seconds) {
		_effectController.Activate( seconds );
		StartCoroutine(WorkoutRoutine(seconds));
	}

	IEnumerator WorkoutRoutine(float seconds) {
		yield return new WaitForSeconds(seconds);
		
		print("WorkoutRoutine, Checking energy: " + Energy);
		if (Energy <= 0.01f)
			Regenerate();
	}

	private void Regenerate() {
		if (renegerationCoroutine != null)
			return;
		
		_animator.SetTrigger("GetHit1Trigger");
		_animator.SetBool("Stunned", true);

		renegerationCoroutine = StartCoroutine(EnergyRegenerationRoutine());
	}

	private void StopRegeneration() {
		if (renegerationCoroutine != null) {
			StopCoroutine(renegerationCoroutine);
			renegerationCoroutine = null;
		}

		_animator.SetBool("Stunned", false);
	}

	IEnumerator EnergyRegenerationRoutine() {
		while (Energy < 1.0f) {
			Energy += _eneryRegenRate * Time.deltaTime;
			if (Energy > 1.0f)
				Energy = 1.0f;
			yield return null;
		}

		StopRegeneration();
	}
}
