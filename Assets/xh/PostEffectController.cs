﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostEffectController : MonoBehaviour {
	public PostProcessingProfile profile;

	public void Activate(float duration) {
		SetEffectActive(true);
		StartCoroutine(AutoOffRoutine(duration));
	}

	private void SetEffectActive(bool active) {
		profile.depthOfField.enabled = active;
	}

	IEnumerator AutoOffRoutine(float delay) {
		yield return new WaitForSeconds( delay );

		SetEffectActive(false);
	}
}
