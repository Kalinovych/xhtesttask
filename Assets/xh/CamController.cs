﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CamController : MonoBehaviour {

	[SerializeField]
	private Transform _lookTarget;

	[SerializeField]
	[Range(0f, 20f)]
	private float _rotationSpeed = 10f;

	private Camera mCamera;

	void Start() {
		mCamera = GetComponent<Camera>();
		mCamera.transform.LookAt(_lookTarget);
	}

	void Update () {
		mCamera.transform.RotateAround(
			_lookTarget.transform.position,
			Vector3.up,
			-Mathf.PI * Time.deltaTime * _rotationSpeed
			);
	}
}
