﻿using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour {

	public HeroController hero;

	public RectTransform fill;

	void Update () {
		var anchorMax = fill.anchorMax;
		anchorMax.x = hero.Energy;
		fill.anchorMax = anchorMax;
	}
}
