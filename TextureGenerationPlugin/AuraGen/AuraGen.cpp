// AuraGen.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "AuraGen.h"
#include <math.h>

// square aura size in pixels
int AuraSize = 128;

extern "C" {
	void SetAuraSize(UINT size) {
		AuraSize = size;
	}

	float DrawAura(char* buffer, float t) {
		UINT pixelCount = AuraSize * AuraSize;
		UINT byteCount = pixelCount * 4;
		
		// clear
		for (UINT i = 0; i < byteCount; i+=4) {
			const float clearRate = 0.9f;
			buffer[i] *= clearRate;
			buffer[i+1] *= clearRate;
			buffer[i+2] *= clearRate;
			buffer[i+3] *= clearRate;
		}

		// draw
		int cx = AuraSize * 0.5;
		int cy = AuraSize * 0.5;
		for (int i = 0; i < AuraSize; i+=2) {
			int x = cx + cos(i + t) * cx;
			int y = cy + sin(i + t) * cy;
			
			SetAuraPix(buffer, i, y, 0xFF, 0x7F, 0x1A, 0xE5);
		}

		return t;
	}
}

static void SetAuraPix(char* buffer, UINT x, UINT y, BYTE a, BYTE r, BYTE g, BYTE b) {
	UINT i = (y * AuraSize + x) * 4;
	buffer[i] = a;
	buffer[i + 1] = r;
	buffer[i + 2] = g;
	buffer[i + 3] = b;
}