#pragma once

#define EXPORT _declspec(dllexport)

/*typedef struct Color {
	BYTE r;
	BYTE g;
	BYTE b;
	BYTE a;
};*/

extern "C" EXPORT void SetAuraSize(UINT size);
extern "C" EXPORT float DrawAura(char* buffer, float t);

static void SetAuraPix(char* buffer, UINT x, UINT y, BYTE a, BYTE r, BYTE g, BYTE b);
